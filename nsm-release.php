<?php
/*
Plugin Name:  NSM - Release
Description:  NSM - Release.
Version:      1.0.0
Author:       Marlow Perceval
License:      MIT License
*/

add_action('init', 'create_nsm_release', 0);
function create_nsm_release() {

	register_taxonomy('nsm_release_type', 'nsm_release', array(
		'hierarchical' => false,
		'labels' => array(
			'name' => _x( 'Release Types', 'taxonomy general name' ),
			'singular_name' => _x( 'Release Type', 'taxonomy singular name' ),
			'search_items' => __( 'Search Release Types' ),
			'popular_items' => __( 'Popular Release Types' ),
			'all_items' => __( 'All Release Types' ),
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => __( 'Edit Release Type' ),
			'update_item' => __( 'Update Release Type' ),
			'add_new_item' => __( 'Add New Release Type' ),
			'new_item_name' => __( 'New Release Type Name' ),
			'separate_items_with_commas' => __( 'Separate release types with commas' ),
			'add_or_remove_items' => __( 'Add or remove release types' ),
			'choose_from_most_used' => __( 'Choose from the most used release types' ),
			'not_found' => __( 'No release types found.' ),
			'menu_name' => __( 'Release Types' ),
		),
		'show_admin_column' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => 'release_type',
		'rewrite' => array('slug' => 'release-type')
	));

	register_taxonomy('nsm_release_genre', 'nsm_release', array(
		'hierarchical' => false,
		'labels' => array(
			'name' => _x( 'Release Genres', 'taxonomy general name' ),
			'singular_name' => _x( 'Release Genre', 'taxonomy singular name' ),
			'search_items' => __( 'Search Release Genres' ),
			'popular_items' => __( 'Popular Release Genres' ),
			'all_items' => __( 'All Release Genres' ),
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => __( 'Edit Release Genre' ),
			'update_item' => __( 'Update Release Genre' ),
			'add_new_item' => __( 'Add New Release Genre' ),
			'new_item_name' => __( 'New Release Genre Name' ),
			'separate_items_with_commas' => __( 'Separate release genres with commas' ),
			'add_or_remove_items' => __( 'Add or remove release genres' ),
			'choose_from_most_used' => __( 'Choose from the most used release genres' ),
			'not_found' => __( 'No release genres found.' ),
			'menu_name' => __( 'Release Genres' ),
		),
		'show_admin_column' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => 'release_genre',
		'rewrite' => array('slug' => 'release-genre'),
	));

	register_taxonomy('nsm_release_year', 'nsm_release', array(
		'hierarchical' => false,
		'labels' => array(
			'name' => _x( 'Release Years', 'taxonomy general name' ),
			'singular_name' => _x( 'Release Year', 'taxonomy singular name' ),
			'search_items' => __( 'Search Release Years' ),
			'popular_items' => __( 'Popular Release Years' ),
			'all_items' => __( 'All Release Years' ),
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => __( 'Edit Release Year' ),
			'update_item' => __( 'Update Release Year' ),
			'add_new_item' => __( 'Add New Release Year' ),
			'new_item_name' => __( 'New Release Year' ),
			'separate_items_with_commas' => __( 'Separate release years with commas' ),
			'add_or_remove_items' => __( 'Add or remove release years' ),
			'choose_from_most_used' => __( 'Choose from the most used release years' ),
			'not_found' => __( 'No release years found.' ),
			'menu_name' => __( 'Release Years' ),
		),
		'show_admin_column' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => 'release_year',
		'rewrite' => array('slug' => 'release-year'),
	));

	register_post_type('nsm_release',
		array(
			'labels' => array(
				'name' => __('Releases'),
				'singular_name' => __('Release'),
				'menu_name' => __('Releases'),
				'all_items' => __('All Releases'),
				'add_new_item' => __('Add New Release'),
				'edit_item' => __('Edit Release'),
				'new_item' => __('New Release'),
				'view_item' => __('View Release'),
				'search_items' => __('Search Releases'),
				'not_found' => __('No releases found'),
				'not_found_in_trash' => __('No releases found in Trash'),
			),
			'public' => true,
			'exclude_from_search' => false,
			'taxonomies' => array(
				'nsm_release_type',
				'nsm_release_genre',
				'nsm_release_year'
			),
			'has_archive' => true,
			'menu_position' => 20,
			'menu_icon'   => 'dashicons-album',
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'excerpt'
			),
			'rewrite' => array(
				'slug' => 'music',
				'with_front' => false
			)
		)
	);
}